import * as React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';

export default function ThreeScreen({navigation}) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Three Screen</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
      <EditScreenInfo path="/screens/TabOneScreen.tsx" />
      <View>
        <TouchableOpacity onPress={()=> navigation.navigate("Root")}>
          <Text>
            ROOT
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{height: 20}}/>
      <View>
        <TouchableOpacity onPress={()=> navigation.navigate("Root", {screen: 'TabOne'})}>
          <Text>
            ONE
          </Text>
        </TouchableOpacity>
      </View>
      <View style={{height: 20}}/>
      <View>
        <TouchableOpacity onPress={()=> navigation.navigate("Root", {screen: 'TabTwo'})}>
          <Text>
            TWO
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
